extends Label
export var xmin = 0
export var xmax = 0
export var ymax = 0
export var ymin = 0

func fade_out():
	hide()

func fade_in():
	show()


func _ready():
	fade_out()

func _on_Player_place(current_pos):
	if current_pos.x > xmin:
		if current_pos.x < xmax:
			if current_pos.y > ymax:
				if current_pos.y < ymin:
					fade_in()
				else:
					 fade_out()
			else:
				 fade_out()
		else:
			 fade_out()
	else:
		 fade_out()
	