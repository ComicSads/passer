extends StaticBody2D
const WHITE = Color(1,1,1)
const BLACK = Color(0,0,0)
const GRAY = Color(0.5,0.5,0.5)
onready var wall_sprite = get_node("Sprite")
onready var wall = get_node(".")
export var Tag = "B"


func _ready():
	if Tag == "B":
		wall_sprite.set_modulate(BLACK)
		set_collision_layer(1)
	elif Tag =="W":
		wall_sprite.set_modulate(WHITE)
		set_collision_layer(2)
	elif Tag =="G":
		wall_sprite.set_modulate(GRAY)
		set_collision_mask(1)