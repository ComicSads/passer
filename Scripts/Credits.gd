extends KinematicBody2D
export var level = 1
const UP = Vector2(0,-1)
const WHITE = Color(1,1,1)
const BLACK = Color (0,0,0)
var motion = Vector2()
var current_pos = Vector2()
var current_color="b"
export var level_1_end = 5600
export var level_2_end = 5500
export var level_3_end = 4500
export var level_4_end = -2100
export var movespeed = 200
export var gravity = 10
export var jump_force = -300
export var respawn_depth = 2000
onready var player_sprite = get_node("Sprite")
onready var player = get_node(".")
signal place
signal level

#func change_color():
#	if current_color == "b":
#		current_color = "w"
#		player_sprite.set_modulate(WHITE)
#		set_collision_mask(2)
#	elif current_color == "w":
#		current_color = "b"
#		player_sprite.set_modulate(BLACK)
#		set_collision_mask(1)

#func next_level():
#	match level:
#		1:
#			if position.x > level_1_end:
#				level = 2
#				get_tree().change_scene("res://l2.tscn")
#		2:
#			if position.x > level_2_end:
#				level = 3
#				get_tree().change_scene("res://l3.tscn")
#		3:
#			if position.x > level_3_end:
#				level = 4
#				get_tree().change_scene("res://cutscene.tscn")
#				
#		4:
#			if position.y < level_4_end:
#				#level = 5
#				get_tree().change_scene("res://final.tscn")
#				pass


func _physics_process(delta):
	motion.y += gravity
#	if Input.is_action_pressed("ui_right"):
#		motion.x = movespeed
#		
#	elif Input.is_action_pressed("ui_left"):
#		motion.x = -movespeed
#		
#	else:
#		motion.x = 0
	position = get_position()
	current_pos = position
	emit_signal("place", current_pos)
#	if is_on_floor():
#		if Input.is_action_just_pressed("ui_up"):
#			motion.y = jump_force
#	if position.y > respawn_depth:
#		get_tree().reload_current_scene()
	motion = move_and_slide(motion, UP)

#func _process(delta):
#	if Input.is_action_just_pressed("ui_cancel"):
#		get_tree().reload_current_scene()
#	if Input.is_action_just_pressed("ui_accept"):
#	next_level()
#	emit_signal("level", level)
