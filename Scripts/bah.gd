extends KinematicBody2D
var motion = Vector2()
const UP = Vector2(0,-1)
export var movespeed = 500
export var gravity = 10
export var jump_force = -300
var lv = 0
export var wait_for_player = 1472
export var stop01 =2048

func update_motion():
	motion = move_and_slide(motion, UP)

func jump():
	motion.y = jump_force
	update_motion()

func left(goal):
	while motion.x > goal:
		motion.x = -movespeed
		update_motion()

func right(goal):
	while motion.x < goal:
		motion.x = movespeed
		update_motion()

func _ready():
	right(1632)


func _physics_process(delta):
	motion.y += gravity
	update_motion()



func _on_Player_place(place):
	if place.x > wait_for_player:
		right(1920)

func _on_Player_level(level):
	lv = level
