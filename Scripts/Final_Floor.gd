extends StaticBody2D
const WHITE = Color(1,1,1)
const BLACK = Color(0,0,0)
const GRAY = Color(0.5,0.5,0.5)
onready var wall_sprite = get_node("Sprite")
onready var wall = get_node(".")
export var starty = float(3)
var t = Timer.new()

func _ready():
	wall_sprite.set_modulate(GRAY)
	set_collision_mask(1)
	t.set_wait_time(starty)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	wall_sprite.set_modulate(BLACK)
	set_collision_layer(1)
	set_collision_mask(0)




