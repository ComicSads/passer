extends StaticBody2D
const WHITE = Color(1,1,1)
const BLACK = Color(0,0,0)
const GRAY = Color(0.5,0.5,0.5)
onready var wall_sprite = get_node("Sprite")
onready var wall = get_node(".")
export var time = 5
export var tag = "B"
export var add_gray = false
var bw = ["B","W"]
var bwg = ["B","W","G"]
var temp = ''

func setColor():
	if tag == "B":
		wall_sprite.set_modulate(BLACK)
		set_collision_layer(1)
	elif tag =="W":
		wall_sprite.set_modulate(WHITE)
		set_collision_layer(2)
	elif tag =="G":
		wall_sprite.set_modulate(GRAY)
		set_collision_mask(1)
var i=0
func _process(delta):
	while i < time:
		i += 1
	i=0
	if add_gray==false:
		temp = randi()%2
		tag = bw[temp]
	else:
		temp = randi()%3
		tag = bwg[temp]
	setColor()