extends Label
var t = Timer.new()
export var wait_to_start = float(3)
export var time_on_screen = float(3)
export var final = "false"

func fade_out():
	hide()

func fade_in():
	show()

func _ready():
	fade_out()
	t.set_wait_time(wait_to_start)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	fade_in()
	t.set_wait_time(time_on_screen)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	fade_out()
	if final == "l4":
		get_tree().change_scene("res://l4.tscn")
	elif final == "l5":
		pass